# CMake P-NUCLEO-WB55 Blinky example
This blinky example runs on the P-NUCLEO-WB55 eval board from STMicroelectronics (https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

# Prerequisites
To build and run this example on the P-NUCLEO-WB55 you will need
  
  - GNU ARM Embedded toolchain from https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm
  - CMake > 3.0
  - Ninja build
  - STM32CubeProgrammer from https://www.st.com/en/development-tools/stm32cubeprog.html

Add the path to the toolchain bin folder (e.g. /opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/) and STM32CubeProgrammer (e.g. $HOME/STM/STM32CubeProgrammer/bin/) to your PATH environment variable.

    export PATH=$HOME/STM/STM32CubeProgrammer/bin/:/opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/:$PATH

The STM32 HAL and CMSIS library included in this repository are part of the ***STMCube MCU Package for STM32WB*** which you can retrieve from STMicroelectronics at https://www.st.com/en/embedded-software/stm32cubewb.html

# Build Example
The example can be build via CMake

    ❯ mkdir build
    ❯ cd build 
    ❯ cmake -GNinja -DCMAKE_TOOLCHAIN_FILE=../toolchain/armgcc.cmake ..
    -- The C compiler identification is GNU 9.3.1
    -- The ASM compiler identification is GNU
    -- Found assembler: /opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/arm-none-eabi-gcc
    -- Detecting C compiler ABI info
    -- Detecting C compiler ABI info - done
    -- Check for working C compiler: /opt/toolchains/gcc-arm-none-eabi-9-2020-q2-update/bin/    arm-none-eabi-gcc - skipped
    -- Detecting C compile features
    -- Detecting C compile features - done
    -- Configuring done
    -- Generating done
    -- Build files have been written to: <your-source-path>/build
    ❯ ninja

This will generate an ELF (blinky.elf) and a binary (blinky.bin) file for flashing to your board.

# Flashing blinky example
Connect your PC to the right Micro-USB Port of the P-NUCLEO-WB55 and short the ***5V Sources*** Jumper at ***USB STL*** (outright jumper position). First you should erase the flash of your board via

    ❯ STM32_Programmer_CLI -c port=swd -fwdelete   

Maybe you will need to run STM32_programmer_CLI with root/sudo to access the USB interface. Now you can flash the binary via

    ❯ STM32_Programmer_CLI -c port=swd mode=UR -d build/blinky.bin 0x8000000 -v

and after flashing the green (LED2) and red (LED3) should be blinking.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This Documentation is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.