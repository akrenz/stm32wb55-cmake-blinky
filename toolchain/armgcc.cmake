# TOOLCHAIN_DIR AND NANO LIBRARY
SET(TOOLCHAIN_BIN_DIR ${TOOLCHAIN_DIR}/bin)
SET(TOOLCHAIN_INC_DIR ${TOOLCHAIN_DIR}/${TARGET_TRIPLET}/include)
SET(TOOLCHAIN_LIB_DIR ${TOOLCHAIN_DIR}/${TARGET_TRIPLET}/lib)

SET(CMAKE_SYSTEM_NAME Generic)
SET(CMAKE_SYSTEM_PROCESSOR arm)
SET(CMAKE_SYSTEM_PROCESSOR arm)
SET(CMAKE_CROSSCOMPILING 1)

# EXECUTABLE EXTENSION
SET(CMAKE_EXECUTABLE_SUFFIX ".elf")
SET(TARGET_TRIPLET "arm-none-eabi")
SET(CMAKE_C_COMPILER ${TARGET_TRIPLET}-gcc${TOOLCHAIN_EXT} CACHE INTERNAL "C compiler")
SET(CMAKE_CXX_COMPILER ${TARGET_TRIPLET}-g++${TOOLCHAIN_EXT} CACHE INTERNAL "C++ compiler")
SET(CMAKE_ASM_COMPILER ${TARGET_TRIPLET}-gcc${TOOLCHAIN_EXT} CACHE INTERNAL "Assembler compiler")

SET(CMAKE_OBJCOPY ${TARGET_TRIPLET}-objcopy)
SET(CMAKE_OBJDUMP ${TARGET_TRIPLET}-objdump)
SET(CMAKE_SIZE ${TARGET_TRIPLET}-size)
SET(CMAKE_STRIP ${TARGET_TRIPLET}-strip)

message(STATUS "PROJECT_SOURCE_DIR: ${PROJECT_SOURCE_DIR}")
SET(CMAKE_C_FLAGS "-mtune=cortex-m4 -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fno-common -fmessage-length=0 -fomit-frame-pointer -fshort-enums -ffat-lto-objects -flto")
SET(CMAKE_CXX_FLAGS "-mtune=cortex-m4 -mcpu=cortex-m4 -march=armv7e-m -mthumb -mfpu=fpv4-sp-d16 -mfloat-abi=hard -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fno-common -fmessage-length=0 -fomit-frame-pointer -fshort-enums -flto")
SET(CMAKE_EXE_LINKER_FLAGS "-T${CMAKE_SOURCE_DIR}/toolchain/STM32WB55RGVX_FLASH.ld -Wl,-Map=${PROJECT_NAME}.map -lc -lm -Wl,--gc-sections")

SET(CMAKE_C_FLAGS_RELEASE "-Os")
SET(CMAKE_CXX_FLAGS_RELEASE "-Os")

SET(CMAKE_FIND_ROOT_PATH ${TOOLCHAIN_DIR} ${EXTRA_FIND_PATH})
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

add_definitions("-DSTM32WB55xx")

set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)